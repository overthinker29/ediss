const Express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
var session = require('express-session');
var FileStore = require('session-file-store')(session);
var mysql = require("mysql");

var app = Express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(
	session({
		secret: 'SEKRETKI',
		saveUninitialized: true,
		resave: true,
		store: new FileStore(),
	})
);

var server = app.listen(process.env.PORT || 3000, () => {
    var port = server.address().port;
    console.log('Server is listening at %s', port);
});


app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'index.html'));
});


const con = mysql.createConnection({
 	//host: "ediss.ckzpncvfwd9x.us-east-1.rds.amazonaws.com",
 	host:'localhost',
 	port:3306,
	user: "vaibhav",
  	password: "overthinker29",
  	database: "ediss"
});

con.connect(function(err){
  	if(err){
    	console.log('Error connecting to Db');
    	return;
  	}
 	console.log('Connection established');

});



function authenticate(username, password, callback) {
	// Check if user exists and is logged in	
	var doesUserExist = false;
	var isUserLoggedIn = false;
	con.query('SELECT * FROM user Where username = ? and password = ?', 
		[username, password], function(err, rows){
		if(err){
			throw err;
		}
		if(rows && rows[0]){
			doesUserExist = true;
		}
		var message = 'User successfully logged in';
		if(!doesUserExist){
			message = 'Username or password does not match';
		}
		callback ({
			'status':doesUserExist,
			'message': message,
		})
	});
}


app.post('/login',function(req,res){
	authenticate(req.body.username, req.body.password, (isAuthenticated)=>{
		console.log("authenticated: ", isAuthenticated);
		if(!isAuthenticated['status']){
			return(res.end(JSON.stringify(isAuthenticated['message'])));
		}
		console.log("logged in : ", req.session.loggedUser);
		if (typeof req.session.loggedUser === 'undefined' ){
		    req.session.loggedUser = req.body.username;
		    return res.end('You are successfully logged in as ' + req.session.loggedUser);
		}
		else if( req.session.loggedUser === req.body.username ){
			return res.end('You are already logged in as '+req.session.loggedUser);
		}
		else{
			req.session.regenerate(function(err){
				req.session.loggedUser = req.body.username;
				return res.end('You are successfully logged in as '+req.session.loggedUser);
			});
		}
	});
});


app.post('/logout',function(req,res){
	req.session.destroy(function(err) {
		if(err) {
			console.log("Error: "+err);
		} else {
			res.redirect('/');
		}
	});
});


app.post('/add', (req, res) => {
    console.log("maxAge: ",req.session.maxAge);
    var output, num1, num2;
    num1 = req.body.num1;
    num2 = req.body.num2;
	
	if(typeof req.session.loggedUser !== undefined ){
		output = add(num1, num2);	
	}
	else{
		output = {'message': 'Not logged in'};
	}
	res.send(JSON.stringify(output));
});


app.post('/divide', (req, res) => {
    var output, num1, num2;
    num1 = req.body.num1;
    num2 = req.body.num2;
	
	if(typeof req.session.loggedUser !== undefined ){
		output = divide(num1, num2);	
	}
	else{
		output = {'message': 'Not logged in'};
	}
	res.send(JSON.stringify(output));
});


app.post('/multiply', (req, res) => {
    var output, num1, num2;
    num1 = req.body.num1;
    num2 = req.body.num2;
	
	if(typeof req.session.loggedUser !== undefined ){
		output = multiply(num1, num2);	
	}
	else{
		output = {'message': 'Not logged in'};
	}
	res.send(JSON.stringify(output));
});


function validate(num) {
	//Check if number is valid
	if(num && parseInt(num) || num==0){
		return true;
	}
	return false;
}


function add(num1, num2) {
	var output;
	var returnData = {};

	if(validate(num1) && validate(num2)){
		output = num1 + num2;
		returnData = {"message":"The action was successful", "result":output};
	}
	else{
		returnData = {"message":"The numbers you entered are not valid"};
	}
	return returnData;
}

function divide(num1, num2) {
	var output;
	var returnData = {};
	if(validate(num1) && validate(num2) && num2!=0){
		output = parseFloat(num1) / parseFloat(num2);
		returnData = {"message":"The action was successful", "result":output};
	}
	else{
		returnData = {"message":"The numbers you entered are not valid"};
	}
	return returnData;
}


function multiply(num1, num2) {
	var output;
	var returnData = {};
	if(validate(num1) && validate(num2)){
		output = num1 * num2;
		returnData = {"message":"The action was successful", "result":output};
	}
	else{
		returnData = {"message":"The numbers you entered are not valid"};
	}
	return returnData;
}

